/// <reference path="mine.ts" />

enum Status {DEFAULT, OPEN};
enum Flag {DEFAULT, BOMB, QUESTION};

class MineSweeper632 {
    instanceName: string;
    mines: Array<Mine>;
    bombs: Array<Mine>;
    tiles_0: Array<number>;
    n_bombs: number= 6;
    width: number= 8;
    height: number= 8;
    td_cells: Array<HTMLTableCellElement>;
    game_status_cell: HTMLTableCellElement;
    game_status: any= {to_flag: -1, to_open: -1, wrong: -1, gameover: false};    //bijhouden van de game
    

    constructor(instanceName){
        this.instanceName= instanceName;
    }
    new_game() {
        this.n_bombs= 6;
        this.width= 8;
        this.height= 8;

        this.mines= [];
        this.bombs= [];
        this.td_cells= [];
        this.tiles_0= [];
        this.generate();
        this.render();
    }

    generate() {
        this.game_status= {to_flag: this.n_bombs
                        , to_open: this.width * this.height - this.n_bombs
                        , wrong: 0
                        , gameover: false};

        var id= 0;
        var type= 0;
        for(var y= 0; y < this.height; y++) {
            for(var x= 0; x < this.width; x++) {
    			this.mines.push(new Mine(id, type, x, y));
                id++;
            }
        }
        //randomly place the bombs
        var maxid= this.mines.length-1; //of id-1
        while(this.n_bombs > 0) {
            var id= Math.round(Math.random() * maxid);
            if(this.mines[id].is_bomb()) {
                continue;
            }
            this.mines[id].set_type(1);
            this.bombs.push(this.mines[id]);
            this.n_bombs--;
        }
        //correct the bombs_around property for each mine
        for(var i= this.bombs.length-1; i >= 0; i--){
            var crawled= this.crawl(this.bombs[i], []);
            for(var c= crawled.length-1; c >= 0; c--){
                var id1= crawled[c];
                this.mines[id1].add_bombs_around();		//= mines[id1].bombs_around+1;
            }
        }
    }
	/**
     * 
     */
    render(){
        var table: HTMLTableElement= <HTMLTableElement> document.createElement("table");
        var thead= <HTMLTableElement> table.createTHead();
        var tbody= <HTMLTableElement> table.createTBody();

        //header for the game status and new game button
        var hrow= <HTMLTableRowElement> table.tHead.insertRow(0);

        //Aantal bommen to go
        var cell= hrow.insertCell(0);
        cell.setAttribute("colSpan", "2");
        cell.className= "c0";
        cell.innerHTML= this.bombs.length.toString();

        cell.setAttribute('instancename', this.instanceName);

        if(typeof window.addEventListener === 'function') {
            (function (cell: HTMLElement) {
                cell.addEventListener("click", function(e){
                    //give hint
                    var instanceName= this.getAttribute("instancename");
                    var game= eval(instanceName);
                    game.hint();
                });
            })(cell);
        }

        //New game button
        var cell= hrow.insertCell(1);
        cell.setAttribute("colSpan", "3");
        cell.className= "c0";
        cell.innerHTML= "New game";

        cell.setAttribute('instancename', this.instanceName);

        if(typeof window.addEventListener === 'function') {
            (function (cell: HTMLElement) {
                cell.addEventListener("click", function(e){
                    var instanceName= this.getAttribute("instancename");
                    
                    var game= eval(instanceName);
                    game.new_game();
                });
            })(cell);
        }

        //game status: voor gameover en gewonnen spel
        var cell= hrow.insertCell(2);
        cell.setAttribute("colSpan", "3");
        cell.className= "c0";
        cell.innerHTML= "";
        this.game_status_cell= <HTMLTableCellElement>cell;

        //de cellen
        var id= 0;
        for(var y= 0; y < this.height; y++) {
            var hrow= <HTMLTableRowElement> table.tHead.insertRow(-1);
            for(var x= 0; x < this.width; x++) {
                var mine= this.mines[id].properties();
                var cell= hrow.insertCell(0);
                cell.className= "";
                cell.setAttribute('id', mine.id.toString());

/*
                if(this.mines[id].is_bomb()){
                    cell.className= "bomb";
                } else {
                    cell.className= "c"+mine.bombs_around;
                    //cell.innerHTML= mine.bombs_around.toString();
                }
*/
                this.td_cells.push(<HTMLTableCellElement> cell);
                if(typeof window.addEventListener === 'function') {
                    var td= <Mine>this.mines[id];

                    //de cell een attribuut meegeven voor gebruik in de closure
                    cell.setAttribute('instancename', this.instanceName);

                    (function (td: Mine) {
                        //http://stackoverflow.com/questions/8909652/adding-click-event-listeners-in-loop
                        
                        cell.addEventListener("contextmenu", function(e){
                    	    e.preventDefault();
                            //console.log(cell);
                            var instanceName= this.getAttribute("instancename");
                            
                            var game= eval(instanceName);
                            game.contextmenu(td);
//                            minesweeper632.contextmenu(td);
                        });

                        //cell.param= this;
                        
                        cell.addEventListener("mousedown", function(e){
                            if(e.which === 3){
                                //contextmenu, laten afhandelen door contextmenu
//                        	    e.preventDefault();
                                return;
                            } else {

                                //var instanceName= 'minesweeper632';
                                var instanceName= this.getAttribute("instancename");
                                
                                var game= eval(instanceName);
                                game.mousedown(td);
                            }
                        });
                    })(td);
                }

                id++;
            }
        }
        if(document.getElementById("game").contains(table)){
//          document.getElementById("game").removeChild(table);
        }
        document.getElementById("game").innerHTML= "";
        
        document.getElementById("game").appendChild(table);
    }
    /**
     * mousedown on a cell
     */
    mousedown(mine: Mine) {
        if(this.game_status.gameover == true) {
            return;
        }
        if(mine.is_open()) {
			return;
		}
        if(mine.is_bomb()) {
            //gameover
            this.gameover();
            return;
        }

        if(!mine.has_bombs_around()){
	  		//de aangrenzende 0-vlakken openen
	  		this.open_adjacent_0_tiles(mine);
        } else {
            var prop= mine.properties();
            this.td_cells[prop.id].className= "c"+prop.bombs_around+" animated zoomIn";
			this.game_status.to_open--;

            //eventueel een flag neutraliseren
            if(prop.flag == Flag.BOMB) {
                //correctly deflagged
                this.game_status.wrong--;
            }

        }
        mine.set_status(Status.OPEN);
        if(this.is_complete()) {
        }
    }
    /**
     * contextmenu on a cell
     */
    contextmenu(mine: Mine) {
        if(this.game_status.gameover == true) {
            return;
        }
        if(mine.is_open()) {
			return;
		}
        mine.mark();
        var prop= mine.properties();
        if(prop.flag == Flag.BOMB) {
            this.td_cells[prop.id].className= "flag";
            if(mine.is_bomb()) {
                //correctly flagged
                this.game_status.to_flag--;
            } else {
                //wrongly flagged
                this.game_status.wrong++;
            }
        } else if(prop.flag == Flag.QUESTION) {
            this.td_cells[prop.id].className= "question";
            if(mine.is_bomb()) {
                //stupidly deflagged
                this.game_status.to_flag++;
            } else {
                //well deflagged
                this.game_status.wrong--;
            }
        } else {
            this.td_cells[prop.id].className= "";
        }
        this.is_complete();
    }
    /**
     * gameover
     */
    gameover() {
        //show all the bombs
		for(var i= this.bombs.length-1; i >= 0; i--) {
            var prop= this.bombs[i].properties();
            this.td_cells[prop.id].className= "bomb";
//            this.td_cells[prop.id].parentElement.parentElement.className= "animated shake";
	  	}

        this.game_status_cell.innerHTML= "Game Over";
        this.game_status_cell.className= "animated shake";
        this.game_status.gameover= true;
    }
    is_complete() {
        if(this.game_status.to_open == 0) {
            this.won();
        } else if(this.game_status.to_flag == 0 && this.game_status.wrong == 0) {
            this.won();
        }
        //console.log(this.game_status);
    }
    won() {
        this.game_status_cell.innerHTML= "Congrats!"
        this.game_status_cell.className= "animated infinite tada";
    }
    /**
     * mark a bomb for free
     */
    hint() {
        if(this.game_status.gameover == true) {
            return;
        }
        for(var i= 0; i < this.bombs.length; i++) {
            var mine= this.bombs[i];
            var prop= mine.properties();
            if(prop.flag == Flag.BOMB) {
                continue;
            } else if(prop.flag == Flag.DEFAULT) {
                this.contextmenu(mine);
                break;
            } else if(prop.flag == Flag.QUESTION) {
                this.contextmenu(mine);
                this.contextmenu(mine);
                break;
            }
        }
    }
    /**
     * open_adjacent_0_tiles
     */
    open_adjacent_0_tiles(mine: Mine) {
        var prop= mine.properties();
        var id0= prop.id;
        if(this.mines[id0].is_open()){		//reeds open
            return;
        }
        
        var crawled= [];		//tile-0's
        var tiles_0= [];	//crawl 0-tiles only once
        crawled= this.crawl(this.mines[id0], crawled);	//all adjacent tiles of a 0-tile can be opened
        for(var i= 0; i < crawled.length; i++){
            var id1= crawled[i];

            if(!this.mines[id1].has_bombs_around()){
                if(tiles_0.indexOf(id1) == -1){
                    //aha, this friend is a 0-tile, crawl from here
                    crawled= this.crawl(this.mines[id1], crawled);
                    tiles_0.push(id1);
                }
            } else {
                if(crawled.indexOf(id1) == -1){
                    crawled.push(id1);
                }
            }
        }
        //de mine waarmee het begint ook toevoegen
        crawled.push(id0);
       
        for(var i= 0; i < crawled.length; i++){
            var id= crawled[i];
            if(this.mines[id].is_open()){
                continue;
            }
            prop= this.mines[id].properties();
            this.td_cells[id].className= "c"+prop.bombs_around+" animated zoomIn";
            this.mines[id].set_status(Status.OPEN);
            //update the game_status
            this.game_status.to_open--;

            //eventueel een flag neutraliseren
            if(prop.flag == Flag.BOMB) {
                //correctly deflagged
                this.game_status.wrong--;
            }
        }
    }
    
	/**
     * om de id van het vlak te bepalen obv van de x en de y
     */
    calculatePixelId(x, y){
	   return (y * this.width + x);
    }
    /**
    *	| x-1,y-1	| x,y-1	| x+1,y-1 |			ONE 	| TWO	| THREE
	*	| x-1,y		| bomb	| x+1, y	|		FOUR	| BOMB	| SIX
	*	| x-1,y+1	| x,y+1	| x+1,y+1	|		SEVEN	| EIGHT	| NINE
     */
    crawl(mine: Mine, crawled) {
		var prop= mine.properties();
		var id0= prop.id;
	    var x0= prop.x;
	    var y0= prop.y;
	    var ONE, TWO, THREE, FOUR, SIX, SEVEN, EIGHT, NINE= false;
		var around= {'ONE': {x: x0-1, y: y0-1}
					,'TWO': {x: x0, y: y0-1}
					,'THREE': {x: x0+1, y: y0-1}
    				,'FOUR': {x: x0-1, y: y0}
                    ,'SIX': {x: x0+1, y: y0}
                    ,'SEVEN': {x: x0-1, y: y0+1}
                    ,'EIGHT': {x: x0, y: y0+1}
                    ,'NINE': {x: x0+1, y: y0+1}
                    };
        var NORTH= (y0 > 0);
        var EAST= (x0 < this.width-1);
        var SOUTH= (y0 < this.height-1);
        var WEST= (x0 > 0);
			
        ONE= WEST && NORTH;
        TWO= NORTH;
        THREE= EAST && NORTH;
        FOUR= WEST;
        SIX= EAST;
        SEVEN= WEST && SOUTH;
        EIGHT= SOUTH;
        NINE= EAST && SOUTH;
        var keys= ['ONE','TWO','THREE','FOUR','SIX','SEVEN','EIGHT','NINE'];
        for(var i= keys.length-1; i >= 0; i--){
            var NUMBER= eval(keys[i]);
            if(NUMBER){
                var id1= this.calculatePixelId(around[keys[i]].x, around[keys[i]].y);
                if(crawled.indexOf(id1) < 0){
                    crawled.push(id1);
                }
            }
        }
        return crawled;
    }
}

var minesweeper632= new MineSweeper632("minesweeper632");
minesweeper632.new_game();


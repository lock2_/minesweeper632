/**
 * Mine
 */
var Mine = (function () {
    function Mine(id, type, x, y) {
        this.id = id;
        this.type = type;
        this.x = x;
        this.y = y;
        this.bombs_around = 0;
        this.flag = 0;
        this.status = 0;
    }
    Mine.prototype.properties = function () {
        return { id: this.id, x: this.x, y: this.y, bombs_around: this.bombs_around, flag: this.flag };
    };
    /*
    * get en set status
    */
    Mine.prototype.is_open = function () {
        return this.status;
    };
    Mine.prototype.set_status = function (stat) {
        this.status = stat;
    };
    /*
    * get en set type
    */
    Mine.prototype.is_bomb = function () {
        return this.type;
    };
    Mine.prototype.set_type = function (type) {
        this.type = type;
    };
    /*
    * bombs around
    */
    Mine.prototype.has_bombs_around = function () {
        return this.bombs_around > 0;
    };
    Mine.prototype.add_bombs_around = function () {
        return this.bombs_around++;
    };
    /**
     * flag
     */
    Mine.prototype.mark = function () {
        this.flag++;
        if (this.flag > 2) {
            this.flag = 0;
        }
        return this.flag;
    };
    return Mine;
})();

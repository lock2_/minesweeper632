/**
 * Mine
 */
class Mine {
    private bombs_around: number= 0;
    private flag: number= 0;
    private status: number= 0;
    constructor(private id: number
               , private type: number
               , private x: number
               , private y: number) {
    }
    properties() {
        return {id: this.id, x: this.x, y: this.y, bombs_around: this.bombs_around, flag: this.flag};
    }
    /*
    * get en set status
    */
    is_open(): number {
        return this.status;
    }
    set_status(stat: number) {
        this.status= stat;
    }
    /*
    * get en set type
    */
    is_bomb(): number {
        return this.type;
    }
    set_type(type: number) {
        this.type= type;
    }
    /*
    * bombs around
    */
    has_bombs_around(): boolean {
        return this.bombs_around > 0;
    }
    add_bombs_around() {
        return this.bombs_around++;
    }
    /**
     * flag
     */
    mark(): number {
        this.flag++;
        if(this.flag > 2){
            this.flag= 0;
        }
        return this.flag;
    }
}